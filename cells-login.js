{
  const {
    html,
  } = Polymer;
  /**
    `<cells-login>` Description.

    Example:

    ```html
    <cells-login></cells-login>
    ```

    ## Styling
    The following custom properties and mixins are available for styling:

    ### Custom Properties
    | Custom Property     | Selector | CSS Property | Value       |
    | ------------------- | -------- | ------------ | ----------- |
    | --cells-fontDefault | :host    | font-family  |  sans-serif |
    ### @apply
    | Mixins    | Selector | Value |
    | --------- | -------- | ----- |
    | --cells-login | :host    | {} |

    * @customElement
    * @polymer
    * @extends {Polymer.Element}
    * @demo demo/index.html
  */
  class CellsLogin extends Polymer.Element {

    static get is() {
      return 'cells-login';
    }

    static get properties() {
      return {
        email:String,
        password:String
      };
    }

    static get template() {
      return html `
      <style include="cells-login-styles cells-login-shared-styles"></style>
      <slot></slot>
      
          <div>
            <img src="https://www.bbvacontinental.pe/fbin/mult/logoperu_tcm1105-418187.png" width="300" heigth="80">
          </div>
          <cells-molecule-input label="Email" type="email" value={{email}}></cells-molecule-input>
          <cells-molecule-input label="Password" type="password" value={{password}}></cells-molecule-input>
      
          <cells-st-button class="full content-centered">
            <button on-click="onClickLogin">Login</button>
          </cells-st-button>
      `;
    }

    onClickLogin(event) {
      console.log('onClickLogin', this.email, this.password);
      let detail = {
        user: this.email,
        password: this.password
      }

      this.dispatchEvent(new CustomEvent("login-evt"), {
        bubbles: true,
        composed: true,
        detail: detail
      })
      
    }
  }

  customElements.define(CellsLogin.is, CellsLogin);
}